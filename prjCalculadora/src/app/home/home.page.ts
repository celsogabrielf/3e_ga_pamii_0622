import { Component } from '@angular/core';
import { Calculadora } from '../models/Calculadora';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  calculadora: Calculadora
  resposta: number

  constructor() {
    this.calculadora = new Calculadora()
  }

  private calcular(operacao: string){
    this.calculadora.operacao = operacao
    this.resposta = this.calculadora.calcular()
  }

}
